const plugin = require("tailwindcss/plugin")

module.exports = {
  // @see https://tailwindcss.com/docs/upcoming-changes
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  // important: true,
  theme: {
    fontFamily: {
      sans: ['"DM Sans"'],
    },
  },
  variants: {},
  plugins: [
    require("tailwindcss"),
    require("autoprefixer"),
    require("@tailwindcss/line-clamp"),
    require("@ky-is/tailwindcss-plugin-width-height")({ variants: ["responsive"] }),
    plugin(function ({ addVariant }) {
      addVariant("sm-only", "@media screen and (max-width: theme('screens.sm'))") // instead of hard-coded 640px use sm breakpoint value from config. Or anything
    }),
  ],
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
}
