import React from "react"
import { AppProps } from "next/app"

import "../styles/style.scss"

function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default App
