const path = require("path")
const withTM = require("next-transpile-modules")(["ky", "screenfull"])

const {
  BugsnagBuildReporterPlugin,
  BugsnagSourceMapUploaderPlugin,
} = require("webpack-bugsnag-plugins")
const packageJson = require("./package.json")

module.exports = withTM({
  rewrites: async () => {
    return [
      {
        source: "/api/protected/login",
        destination: "http://localhost:3000/api/protected/login",
        basePath: false,
      },
      {
        source: "/api/protected/password-check",
        destination: "http://localhost:3000/api/protected/password-check",
        basePath: false,
      },
    ]
  },
  generateEtags: false,
  poweredByHeader: false,
  api: {
    bodyParser: false,
  },
  experimental: { images: { allowFutureImage: true } },
  images: {
    domains: [
      "cdn-learning.harisenin.com",
      "cdn-beta.harisenin.com",
      "cdn.harisenin.com",
      "ui-avatars.com",
      "singlecolorimage.com",
      "harisenin-learning.s3.ap-southeast-3.amazonaws.com",
      "harisenin-storage.s3-ap-southeast-1.amazonaws.com",
      "harisenin-storage.s3.ap-southeast-1.amazonaws.com",
      "harisenin-storage-dev.s3.ap-southeast-1.amazonaws.com",
    ],
    dangerouslyAllowSVG: true,
  },
  basePath: "/learning",
  productionBrowserSourceMaps: true,
  webpack: (config) => {
    config.module.rules
      .filter((rule) => !!rule.oneOf)
      .forEach((rule) => {
        rule.oneOf
          .filter((oneOfRule) => {
            return oneOfRule.test
              ? oneOfRule.test.toString().includes("sass") &&
                  Array.isArray(oneOfRule.use) &&
                  oneOfRule.use.some((use) => use.loader.includes("sass-loader"))
              : false
          })
          .forEach((rule) => {
            rule.use = rule.use.map((useRule) => {
              if (useRule.loader.includes("sass-loader")) {
                return {
                  ...useRule,
                  options: {
                    ...useRule.options,
                    additionalData: `$cdnUrl: '${process.env.HARISENIN_CDN_URL}';`,
                  },
                }
              }
              return useRule
            })
          })
      })

    if (process.env.HARISENIN_BUGSNAG_RELEASE_STAGE !== "local") {
      config.plugins.push(
        new BugsnagBuildReporterPlugin(
          {
            apiKey: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
            appVersion: packageJson.version,
            releaseStage: process.env.HARISENIN_BUGSNAG_RELEASE_STAGE,
            autoAssignRelease: true,
          },
          {
            /* opts */
          }
        ),
        new BugsnagSourceMapUploaderPlugin({
          apiKey: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
          publicPath: ".next",
          overwrite: true,
          ignoredBundleExtensions: [".css"],
        })
      )
    }

    return config
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  serverRuntimeConfig: {
    // Will only be available on the server side
    API_ENDPOINT: process.env.HARISENIN_API_ENDPOINT,
    WSS_ENDPOINT: process.env.HARISENIN_WSS_ENDPOINT,
    HOME_URL: process.env.HARISENIN_HOME_URL,
    SCHOOL_URL: process.env.HARISENIN_SCHOOL_URL,
    RISEBINAR_URL: process.env.HARISENIN_RISEBINAR_URL,
    CDN_URL: process.env.HARISENIN_CDN_URL,
    XENDIT_KEY: process.env.HARISENIN_XENDIT_KEY,
    LINK_TOKEN: process.env.HARISENIN_LINK_TOKEN,
    BUGSNAG_API_KEY: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
    BUGSNAG_CAPTURE_SESSIONS: process.env.HARISENIN_BUGSNAG_CAPTURE_SESSIONS === "true",
    BUGSNAG_RELEASE_STAGE: process.env.HARISENIN_BUGSNAG_RELEASE_STAGE,
    BUGSNAG_SEND_CODE: process.env.HARISENIN_BUGSNAG_SEND_CODE === "true",
    PASSWORD_PROTECT: process.env.HARISENIN_PASSWORD_PROTECT === "true",
    GOOGLE_CLIENT_ID: process.env.HARISENIN_GOOGLE_CLIENT_ID,
    COOKIES_DOMAIN: process.env.HARISENIN_COOKIES_DOMAIN,
    FACEBOOK_PIXEL: process.env.HARISENIN_FACEBOOK_PIXEL,
    GTM_ID: process.env.HARISENIN_GTM_ID,
    GA_TRACKING_ID: process.env.HARISENIN_GA_TRACKING_ID,
  },
  env: {
    API_ENDPOINT: process.env.HARISENIN_API_ENDPOINT,
    WSS_ENDPOINT: process.env.HARISENIN_WSS_ENDPOINT,
    HOME_URL: process.env.HARISENIN_HOME_URL,
    SCHOOL_URL: process.env.HARISENIN_SCHOOL_URL,
    RISEBINAR_URL: process.env.HARISENIN_RISEBINAR_URL,
    CDN_URL: process.env.HARISENIN_CDN_URL,
    XENDIT_KEY: process.env.HARISENIN_XENDIT_KEY,
    LINK_TOKEN: process.env.HARISENIN_LINK_TOKEN,
    BUGSNAG_API_KEY: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
    BUGSNAG_CAPTURE_SESSIONS: process.env.HARISENIN_BUGSNAG_CAPTURE_SESSIONS,
    BUGSNAG_RELEASE_STAGE: process.env.HARISENIN_BUGSNAG_RELEASE_STAGE,
    BUGSNAG_SEND_CODE: process.env.HARISENIN_BUGSNAG_SEND_CODE,
    PASSWORD_PROTECT: process.env.HARISENIN_PASSWORD_PROTECT,
    GOOGLE_CLIENT_ID: process.env.HARISENIN_GOOGLE_CLIENT_ID,
    COOKIES_DOMAIN: process.env.HARISENIN_COOKIES_DOMAIN,
    FACEBOOK_PIXEL: process.env.HARISENIN_FACEBOOK_PIXEL,
    GTM_ID: process.env.HARISENIN_GTM_ID,
    GA_TRACKING_ID: process.env.HARISENIN_GA_TRACKING_ID,
  },
})
