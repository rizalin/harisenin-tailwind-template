import express, { Request, Response } from "express"
import { join } from "path"
import next from "next"

const dev = process.env.NODE_ENV !== "production"
const app = next({ dev })
const handle = app.getRequestHandler()
const port = process.env.PORT || 3004

;(async () => {
  try {
    await app.prepare()
    const server = express()
    server.all("*", (req: Request, res: Response) => {
      // const parsedUrl = new URL(req.url,`http://localhost:${port}`)
      // const {pathname} = parsedUrl
      // if (pathname === "/sw.js" || /^\/(workbox|worker|fallback)-\w+\.js$/.test(<string>pathname)) {
      //   const filePath = join(process.cwd(), "public", pathname)
      //   return app.serveStatic(req, res, filePath)
      // }

      return handle(req, res)
    })
    server.listen(port, (err?: any) => {
      if (err) throw err
      console.log(`> Ready on localhost:${port} - env ${process.env.NODE_ENV}`)
    })
  } catch (e) {
    console.error(e)
    process.exit(1)
  }
})()
